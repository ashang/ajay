class CandidatesController < ApplicationController
	
	def index
    	@candidates = Candidate.all
    end
	
	def show
    	@candidate = Candidate.find(params[:id])
  	end

  	def edit
	  @candidate = Candidate.find(params[:id])
	end

	def new
	end

	def create
			# render plain: params[:candidate].inspect
		@candidate = Candidate.new(candidate_params)
 		# binding.pry
		is_save = @candidate.save
		# binding.pry
		# redirect_to @candidate
		#is_save ? (redirect_to @candidate) : (flash.now[:notice] = "Post failed created!!!")
		binding.pry
		if is_save
			flash[:notice] = "Post created!!!"
			redirect_to @candidate
		else
			flash[:notice] = "Post created false!!!"
			render :new
		end
	end

	def batch_delete
		# binding.pry
		Candidate.destroy(params[:candidates]) unless params[:candidates].blank?
		redirect_to :back
	end

	def batch_edit
		@candidates = Candidate.find(params[:candidates]) unless params[:candidates].blank?
		# Candidate.destroy(params[:candidates]) unless params[:candidates].blank?
		render 'edits'
	end

	def batch_update
		@candidates_list = params[:candidate_ids] unless params[:candidate_ids].blank?
		@candidates_list.each do |candidate|
			@candidate = Candidate.find(candidate.first)
			@candidate.update_attribute(candidate.last.keys.first, candidate.last.values.first)		
		end
		# Candidate.destroy(params[:candidates]) unless params[:candidates].blank?
		redirect_to action: "index"
		# redirect_to action: "index"
	end

	def destroy
	    @candidate = Candidate.where("id = ?", params[:id]).first
	    @candidate.destroy
		#redirect_to :back
		redirect_to action: "index"
	    #render :index

	     # respond_to do |format|
	     #   format.html { redirect_to :controller => "candidate" , :action => "index" }
	     #   format.xml  { head :ok }
	     # end
  	end
	
	def update
		@candidate = Candidate.find(params[:id])
		# binding.pry
		count = @candidate.attributes["votenum"]
		count = 0 if !count
		@candidate.update_attribute(:votenum, count + 1)
		redirect_to :back
		  # if @candidate.update(candidate_params)
		  #   redirect_to @candidates
		  # else
		  #   render 'index'
		  # end
	end


	private

	  def candidate_params
	    params.require(:candidate).permit(:name)
	  end
	  
end
