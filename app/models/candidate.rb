class Candidate < ActiveRecord::Base

	# validates_of_uniqueness :name
	validates_uniqueness_of :name,:message=>"The name #{name} is taken."
	validates :name, length:{maxinum: 5,minimum: 2}
	# before_validation :candidate_validation

	private
	
	  def candidate_validation
	  	is_exists = Candidate.all.exists?(:name=>self.name)
	  	# binding.pry
	  	!is_exists
	  	# self.message = 1
	  end
end
